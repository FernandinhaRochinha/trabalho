﻿using Canalzinhowf.DB.Canal;
using Canalzinhowf.DB.Programa;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Canalzinhowf.telas
{
    public partial class frmNovo : Form
    {
        public frmNovo()
        {
            InitializeComponent();
            CarregarCombos();
        }
        void CarregarCombos()
        {
            CanalBusiness business = new CanalBusiness();
            List<CanalDTO> lista = business.Listar();

            cbocanais.ValueMember = nameof(CanalDTO.ID);
            cbocanais.DisplayMember = nameof(CanalDTO.Nome);
            cbocanais.DataSource = lista;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void frmNovo_Load(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                CanalDTO canal = cbocanais.SelectedItem as CanalDTO;

                ProgramaDTO dto = new ProgramaDTO();
                dto.Canal = canal.ID;
                dto.Nome = txtnome.Text.Trim();
                dto.Horario = mkthorario.Text.Trim();
                dto.Sinopse = txtsinopse.Text.Trim();

                ProgramaBusiness bs = new ProgramaBusiness();
                bs.Salvar(dto);

                MessageBox.Show("Programa cadastrado com sucesso !");
                frmInicial tela = new frmInicial();
                tela.Show();
                Hide();
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Preencha todos os campos." );
            }
            catch (Exception ex)
            {

                MessageBox.Show("Oh Oh! Algo de errado!");
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmProgramas tela = new frmProgramas();
            tela.Show();
            Hide();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            frmcadstrar tela = new frmcadstrar();
            tela.Show();
            Hide();
        }

        private void txtnome_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtsinopse_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
