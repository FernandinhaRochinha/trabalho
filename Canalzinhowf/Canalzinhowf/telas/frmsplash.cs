﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Canalzinhowf.telas
{
    public partial class frmsplash : Form
    {
        public frmsplash()
        {
            InitializeComponent();

            
            Task.Factory.StartNew(() =>
            {
                
                System.Threading.Thread.Sleep(5000);

                Invoke(new Action(() =>
                {
                   
                    frmLogin tela = new frmLogin();
                    tela.Show();
                    Hide();
                }));
            });
        }
    }
}
