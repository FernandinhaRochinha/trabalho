﻿using Canalzinhowf.DB.Canal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Canalzinhowf.telas
{
    public partial class frmcadstrar : Form
    {
        public frmcadstrar()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void frmcadstrar_Load(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            try
            {
                CanalDTO dto = new CanalDTO();
                dto.Nome = txtnome.Text.Trim();
                dto.Numero = Convert.ToInt32(txtnumero.Text.Trim());

                CanalBusiness businesse = new CanalBusiness();
                businesse.Salvar(dto);

                MessageBox.Show("Canal Salvo com sucesso!");
                this.Hide();

                frmInicial tela = new frmInicial();
                tela.Show();
                Hide();

            }
            //catch (ArgumentException)
            //{
            //    MessageBox.Show("Preencha todos os campos.");
            //}
            catch (Exception )
            {

               MessageBox.Show("Tente preencher todos os campos");
            }
            
            


        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmCanalListar tela = new frmCanalListar();
            tela.Show();
            Hide();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            frmInicial tela = new frmInicial();
            tela.Show();
            Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmNovo tela = new frmNovo();
            tela.Show();
            Hide();
        }
    }
}
