﻿using Canalzinhowf.DB.Canal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Canalzinhowf.telas
{
    public partial class frmAlterar : Form
    {
        public frmAlterar()
        {
            InitializeComponent();
        }

        CanalDTO dto = new CanalDTO(); 

        public void LoadScreen (CanalDTO dto)
        {
            lblid.Text = dto.ID.ToString();
            txtnome.Text = dto.Nome;
            nudnumero.Value = dto.Numero;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                this.dto.ID = int.Parse(lblid.Text.Trim());
                this.dto.Nome = txtnome.Text.Trim();
                this.dto.Numero = Convert.ToInt32(nudnumero.Value);

                CanalBusiness bs = new CanalBusiness();
                bs.Alterar(dto);

                MessageBox.Show("canal alterado com sucesso.");

                frmCanalListar tela = new frmCanalListar();
                tela.Show();
                Close();
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Todos os campos devem estar preenchidos.");
            }
            catch (Exception ex)
            {


                MessageBox.Show("OH OH!, ocorreu um erro! ");
            }
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();

        }
    }
}
