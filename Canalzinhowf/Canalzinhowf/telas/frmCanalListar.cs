﻿using Canalzinhowf.DB.Canal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Canalzinhowf.telas
{
    public partial class frmCanalListar : Form
    {
        public frmCanalListar()
        {
            InitializeComponent();
            CanalBusiness busines = new CanalBusiness();
            List<CanalDTO> listar = busines.Listar();

            dgvcanais.AutoGenerateColumns = false;
            dgvcanais.DataSource = listar;


        }

        private void frmCanalListar_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmcadstrar tela = new frmcadstrar();
            tela.Show();
            Hide();
             
        }

        private void button1_Click(object sender, EventArgs e)
        {

            try
            {
                
                    
               

                CanalDTO dto = new CanalDTO();
                dto.Nome = txtconsultar.Text.Trim();
                CanalBusiness business = new CanalBusiness();
                List<CanalDTO> lista = business.Consultar(dto);

                dgvcanais.AutoGenerateColumns = false;
                dgvcanais.DataSource = lista;
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Preencha todos os campos.");
            }
            catch (Exception)
            {

                MessageBox.Show("Oh Oh! Algo de errado!");
            }
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmInicial tela = new frmInicial();
            tela.Show();
            Hide();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void dgvcanais_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dgvcanais_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 4)
                {
                    CanalDTO canal = dgvcanais.Rows[e.RowIndex].DataBoundItem as CanalDTO;

                    frmAlterar tela = new frmAlterar();
                    tela.LoadScreen(canal);
                    tela.ShowDialog();
                    Hide();

                    
                    
                }

                if (e.ColumnIndex == 3)
            {
                CanalDTO canal = dgvcanais.Rows[e.RowIndex].DataBoundItem as CanalDTO;

                DialogResult r = MessageBox.Show($"Deseja realmente excluir o canal {canal.ID}?", "Canalzinho",
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    CanalBusiness business = new CanalBusiness();
                    business.Remover(canal.ID);

                    button1_Click(null, null);
                }
            }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Canalzinho",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "Canalzinho",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            CanalBusiness busines = new CanalBusiness();
            List<CanalDTO> listar = busines.Listar();

            dgvcanais.AutoGenerateColumns = false;
            dgvcanais.DataSource = listar;
        }
    }
}
