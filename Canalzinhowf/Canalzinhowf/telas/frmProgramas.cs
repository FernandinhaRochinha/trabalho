﻿using Canalzinhowf.DB.Canal;
using Canalzinhowf.DB.Programa;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Canalzinhowf.telas
{
    public partial class frmProgramas : Form
    {
        public frmProgramas()
        {
            InitializeComponent();


        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void frmProgramas_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmNovo tela = new frmNovo();
            tela.Show();
            Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            try
            {
                ProgramaView view = new ProgramaView();

            view.Programa = txtbuscar.Text.Trim();

            ProgramaBusiness bs = new ProgramaBusiness();
            List<ProgramaView> lista = bs.Consultar(view);

            dgvprograma.AutoGenerateColumns = false;
            dgvprograma.DataSource = lista;

            }
            catch (Exception)
            {

                MessageBox.Show("Oh Oh! Algo de errado!");
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmInicial tela = new frmInicial();
            tela.Show();
            Hide();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            ProgramaBusiness busines = new ProgramaBusiness();
            List<ProgramaView> listar = busines.Listar();

            dgvprograma.AutoGenerateColumns = false;
            dgvprograma.DataSource = listar;
        }

        private void dgvprograma_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 5)
            {
                ProgramaView programa = dgvprograma.Rows[e.RowIndex].DataBoundItem as ProgramaView;


                DialogResult r = MessageBox.Show("Deseja realmente excluir o programa ", "Canalzinho",
                                    MessageBoxButtons.YesNo);

                if (r == DialogResult.Yes)
                {
                    ProgramaBusiness business = new ProgramaBusiness();
                    business.Remover(programa.ID);

                    ProgramaBusiness busines = new ProgramaBusiness();
                    List<ProgramaView> listar = busines.Listar();

                    dgvprograma.AutoGenerateColumns = false;
                    dgvprograma.DataSource = listar;

                }
            }
        }
    }
}
