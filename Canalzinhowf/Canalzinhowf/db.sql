-- MySQL Script generated by MySQL Workbench
-- Thu Aug 23 18:29:55 2018
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`tb_canal`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`tb_canal` (
  `id_canal` INT NOT NULL AUTO_INCREMENT,
  `nm_canal` VARCHAR(45) NOT NULL,
  `vl_canal` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_canal`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`tb_programa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`tb_programa` (
  `id_programa` INT NOT NULL AUTO_INCREMENT,
  `nm_programa` VARCHAR(45) NOT NULL,
  `hr_programacol` VARCHAR(45) NOT NULL,
  `nm_programacol` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_programa`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`table3`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`table3` (
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`tb_canal_has_tb_programa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`tb_canal_has_tb_programa` (
  `tb_canal_id_canal` INT NOT NULL,
  `tb_programa_id_programa` INT NOT NULL,
  PRIMARY KEY (`tb_canal_id_canal`, `tb_programa_id_programa`),
  INDEX `fk_tb_canal_has_tb_programa_tb_programa1_idx` (`tb_programa_id_programa` ASC),
  INDEX `fk_tb_canal_has_tb_programa_tb_canal_idx` (`tb_canal_id_canal` ASC),
  CONSTRAINT `fk_tb_canal_has_tb_programa_tb_canal`
    FOREIGN KEY (`tb_canal_id_canal`)
    REFERENCES `mydb`.`tb_canal` (`id_canal`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tb_canal_has_tb_programa_tb_programa1`
    FOREIGN KEY (`tb_programa_id_programa`)
    REFERENCES `mydb`.`tb_programa` (`id_programa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
