﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Canalzinhowf.DB.Canal
{
    public class CanalBusiness
    {
        CanalDataBase db = new CanalDataBase();

        public int Salvar (CanalDTO dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("O nome do canal nao pode ficar vazio");
            }
            if (dto.Numero == 0)
            {
                throw new ArgumentException("O numero do canal é obrigatorio");
            }
           
            return db.Salvar(dto);
        }
        public List<CanalDTO> Consultar(CanalDTO dto)
        {          
            return db.Consultar(dto);
        }
        public List<CanalDTO> Listar()
        {
            return db.Listar();
        }
        public void Remover (int id)
        {
            db.Remover(id);
        }
        public void Alterar (CanalDTO dto)
        {
            db.Alterar(dto);
        }
    }
}
