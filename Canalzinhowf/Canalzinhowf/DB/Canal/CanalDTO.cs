﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Canalzinhowf.DB.Canal
{
    public class CanalDTO
    {
        public int ID { get; set; }

        public string Nome { get; set; }

        public int Numero { get; set; }
    }
}
