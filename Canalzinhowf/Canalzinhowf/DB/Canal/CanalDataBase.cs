﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Canalzinhowf.DB.Canal
{
    public class CanalDataBase
    {
        public int Salvar (CanalDTO dto)
        {
              string script = @"INSERT INTO tb_canal (nm_canal , vl_canal) VALUES (@nm_canal , @vl_canal)";

                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("nm_canal", dto.Nome));
                parms.Add(new MySqlParameter("vl_canal", dto.Numero));

                Database db = new Database();
                return db.ExecuteInsertScriptWithPk(script, parms);
           
        }

        public List<CanalDTO> Listar()
        {
            string script = @"SELECT * FROM tb_canal";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<CanalDTO> listar = new List<CanalDTO>();
            while (reader.Read())
            {
                CanalDTO dto = new CanalDTO();
                dto.ID = reader.GetInt32("id_canal");
                dto.Nome = reader.GetString("nm_canal");
                dto.Numero = reader.GetInt32("vl_canal");

                listar.Add(dto);
            }
            reader.Close();

            return listar;

        }

        public List<CanalDTO> Consultar(CanalDTO dto)
        {
            string script =
                @"SELECT * FROM tb_canal 
                           WHERE nm_canal like @nm_canal";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_canal", "%" + dto.Nome + "%"));

            Database db = new Database();

            MySqlDataReader r = db.ExecuteSelectScript(script, parms);

            List<CanalDTO> lista = new List<CanalDTO>();
            while (r.Read())
            {
                CanalDTO dtos = new CanalDTO();
                dtos.ID = r.GetInt32("id_canal");
                dtos.Nome = r.GetString("nm_canal");
                dtos.Numero = r.GetInt32("vl_canal");
                lista.Add(dtos);
            }
            r.Close();
            return lista;
        }
        public void Remover(int ID)
        {
            string script = @"DELETE FROM tb_canal WHERE id_canal = @id_canal";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_canal", ID));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public void Alterar (CanalDTO dto)
        {
            string script = @"UPDATE tb_canal
                                SET nm_canal = @nm_canal , 
                                    id_canal = @id_canal,
                                    vl_canal = @vl_canal
                                WHERE id_canal = @id_canal";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_canal", dto.ID));
            parms.Add(new MySqlParameter("nm_canal", dto.Nome));
            parms.Add(new MySqlParameter("vl_canal", dto.Numero));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);


        }
    }
}
