﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Canalzinhowf.DB.Login
{
   public  class LoginBusiness
    {
        LoginDatabase db = new LoginDatabase();

        public bool Login (string Nome , string Senha)
        {
            if (Nome == string.Empty)
            {
                throw new ArgumentException("Usuario é obrigatorio");
            }

            if (Senha == string.Empty)
            {
                throw new ArgumentException("Senha é obrigatoria");
            }

            bool login = db.Login(Nome, Senha);
            return login;

            
        }
    }
}
