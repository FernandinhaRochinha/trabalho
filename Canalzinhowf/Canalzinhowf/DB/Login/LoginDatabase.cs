﻿using Canalzinhowf.DB.Login;
using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Canalzinhowf.DB
{
    public class LoginDatabase
    {
        public bool Login (string Nome , string Senha)
        {
            string script = @"SELECT * FROM tb_login 
                              WHERE nm_usuario = @nm_usuario 
                              AND ds_senha = @ds_senha";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_usuario", Nome));
            parms.Add(new MySqlParameter("ds_senha", Senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            if (reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
