﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Canalzinhowf.DB.Login
{
    public class LoginDTO
    {
        public int ID { get; set; }

        public string Nome { get; set; }

        public string Senha { get; set; }

        public bool Permissao { get; set;  }
    }
}
