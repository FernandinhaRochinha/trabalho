﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Canalzinhowf.DB.Programa
{
    public class ProgramaView
    {
        public int ID { get; set; } 

        public string Programa { get; set; }

        public String  Horario { get; set; }

        public string Sinopse { get; set; }

        public string Canal { get; set; }
    }
}
