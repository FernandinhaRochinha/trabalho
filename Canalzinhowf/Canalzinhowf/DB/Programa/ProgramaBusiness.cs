﻿using Canalzinhowf.DB.Canal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Canalzinhowf.DB.Programa
{
    public class ProgramaBusiness
    {
        
        ProgramaDataBase db = new ProgramaDataBase();

        public int Salvar (ProgramaDTO dto)
        {
            if (dto.Nome == string.Empty )
            {
                throw new ArgumentException("O nome do programa é obrigatorio");
            }

            if (dto.Horario == string.Empty)
            {
                throw new ArgumentException("O horario do programa é obrigatorio");
            }

            if (dto.Sinopse == string.Empty)
            {
                throw new ArgumentException("A sinopse do programa é obrigatoria");
            }

            if (dto.Canal == 0)
            {
                throw new ArgumentException("O nome do Canal é obrigatorio");
            }

            return db.Salvar(dto);
        }
        public List<ProgramaView> Listar()
        {
            return db.Listar();
        }
        public List<ProgramaView> Consultar (ProgramaView view)
        {
            return db.Consultar(view);
        }
        public void Remover(int ID)
        {
             db.Remover(ID); 
        }


    }
}
