﻿using Canalzinhowf.DB.Programa;
using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Canalzinhowf.DB.Canal
{
    public class ProgramaDataBase
    {
        public int Salvar(ProgramaDTO dto)
        {

            string script = @"INSERT INTO tb_programa (nm_programa , hr_programa , nm_sinopse , id_canal)
                  VALUES (@nm_programa , @hr_programa , @nm_sinopse , @id_canal)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_programa", dto.Nome));
            parms.Add(new MySqlParameter("hr_programa", dto.Horario));
            parms.Add(new MySqlParameter("nm_sinopse", dto.Sinopse));
            parms.Add(new MySqlParameter("id_canal", dto.Canal));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
        public List<ProgramaView> Listar()
        {
            string script = @"select * FROM view_canal_programa";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProgramaView> lista = new List<ProgramaView>();
            while (reader.Read())
            {
                ProgramaView dto = new ProgramaView();
                dto.ID = reader.GetInt32("id_programa");
                dto.Programa = reader.GetString("nm_programa");
                dto.Horario = reader.GetString("hr_programa");
                dto.Sinopse = reader.GetString("nm_sinopse");
                dto.Canal = reader.GetString("nm_canal");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }
        public List<ProgramaView> Consultar(ProgramaView view)
        {
            string script = @"select * FROM view_canal_programa WHERE nm_programa like @nm_programa";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_programa", "%" + view.Programa + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProgramaView> lista = new List<ProgramaView>();
            while (reader.Read())
            {
                ProgramaView dto = new ProgramaView();
                dto.ID = reader.GetInt32("id_programa");
                dto.Programa = reader.GetString("nm_programa");
                dto.Horario = reader.GetString("hr_programa");
                dto.Sinopse = reader.GetString("nm_sinopse");
                dto.Canal = reader.GetString("nm_canal");

                lista.Add(dto);
            }
            reader.Close();
            return lista;

        }
        public void Remover(int ID)
        {
            string script = @"DELETE FROM tb_programa WHERE id_programa = @id_programa";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_programa", ID));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
    }
}
