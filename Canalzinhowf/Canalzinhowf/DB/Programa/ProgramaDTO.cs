﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Canalzinhowf.DB.Programa
{
    public class ProgramaDTO
    {
        public int ID { get; set; }


        public string Nome { get; set; }

        public string Horario { get; set; }

        public string Sinopse { get; set; }

        public int Canal { get; set;  }
    }
}
