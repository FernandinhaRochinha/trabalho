﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Canalzinho.login
{
    public class loginDatabase
    {
        public int Salvar (loginDTO dto)
        {
            string script = @"INSERT INTO tb_login (nm_usuario , sn_senha ) 
                            VALUES (@nm_usuario , @sn_senha)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_usuario", dto.Usuario)); 
            parms.Add(new MySqlParameter("sn_senha", dto.Senha));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

            
        }
    }
}
